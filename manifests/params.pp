class apache::params {
  $package_ensure    = "present"
  $package_name      = "apache2"
  $service_name      = "apache2"
  $service_ensure    = "running"
  $php_package       = "libapache2-mod-php5"
  $file_ensure       = "present"
  $file_site_dest    = "/etc/apache2/sites-enabled/000-default"
  $file_rewrite_dest = "/etc/apache2/mods-enabled/rewrite.load"
  $file_site_src     = "puppet:///modules/apache/000-default"
  $file_rewrite_src  = "puppet:///modules/apache/rewrite.load"
}

