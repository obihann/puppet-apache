class apache::service (
  $package_name      = $apache::params::package_name,
  $service_ensure    = $apache::params::service_ensure,
  $service_name      = $apache::params::service_name,
) inherits apache::params {
    service { $service_name:
        ensure  => $service_ensure,
        require => Package[$package_name],
    }
}

