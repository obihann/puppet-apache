class apache::config (
  $package_name      = $apache::params::package_name,
  $service_name      = $apache::params::service_name,
  $file_ensure       = $apache::params::file_ensure,
  $file_site_src     = $apache::params::file_site_src,
  $file_site_dest    = $apache::params::file_site_dest,
  $file_rewrite_src  = $apache::params::file_rewrite_src,
  $file_rewrite_dest = $apache::params::file_rewrite_dest,
) inherits apache::params {
    file { $file_site_dest:
        ensure => $file_ensure,
        source => [$file_site_src],
        require => Package[$package_name],
        notify => Service[$service_name],
    }

    file { $file_rewrite_dest:
        ensure => $file_ensure,
        source => [$file_rewrite_src],
        require => Package[$package_name],
        notify => Service[$service_name],
    }
}

