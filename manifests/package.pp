class apache::package (
  $package_ensure    = $apache::params::package_ensure,
  $package_name      = $apache::params::package_name,
  $service_name      = $apache::params::service_name,
) inherits apache::params {
  package { $package_name:
    ensure => $package_ensure,
    notify => Service[$service_name],
  }
}

