class apache::php (
  $package_name      = $apache::params::package_name,
  $package_ensure    = $apache::params::package_ensure,
  $service_name      = $apache::params::service_name,
  $php_package       = $apache::params::php_package,
) inherits apache {
    package { $php_package:
        ensure  => $package_ensure,
        require => Package[$package_name],
        notify  => Service[$service_name],
    }
}
