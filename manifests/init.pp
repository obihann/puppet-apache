class apache (
  $package_ensure     = $apache::params::package_ensure,
  $package_name       = $apache::params::package_name,
  $service_ensure     = $apache::params::service_ensure,
  $service_name       = $apache::params::service_name,
  $file_ensure        = $apache::params::file_ensure,
  $file_site_src      = $apache::params::file_site_src,
  $file_site_dest     = $apache::params::file_site_dest,
  $file_rewrite_src   = $apache::params::file_rewrite_src,
  $file_rewrite_dest  = $apache::params::file_rewrite_dest,
) inherits apache::params {
  class { "::apache::package":
    package_ensure    => $package_ensure,
    package_name      => $package_name,
    service_name      => $service_name,
  }

  class { "::apache::service":
    package_name      => $package_name,
    service_name      => $service_name,
    service_ensure    => $service_ensure,
  }

  class { "::apache::config":
    package_name      => $package_name,
    service_name      => $service_name,
    file_ensure       => $file_ensure,
    file_site_src     => $file_site_src,
    file_site_dest    => $file_site_dest,
    file_rewrite_src  => $file_rewrite_src,
    file_rewrite_dest => $file_rewrite_dest,
  }
}
