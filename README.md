# Puppet Apache

This is a basic puppet module that installs apache2, and configures mod_rewrite.

## Basic Usage
By default puppet-apache will include apache2, enable mod_rewrite, and update the default site to allow
for .htaccess overrides.

```
include apache
```

## PHP Support
To install apache2 with the  `libapache2-mod-php5` module.

```
include apache::php
```
